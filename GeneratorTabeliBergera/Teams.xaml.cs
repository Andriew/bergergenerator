﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;

namespace GeneratorTabeliBergera
{
    /// <summary>
    /// Interaction logic for Teams.xaml
    /// </summary>
    public partial class Teams : Window
    {
        private readonly List<TeamContainer> _teamsList;
        private DateTime? _date;
        private bool _revenge = false;

        public Teams(List<TeamContainer> teamsList)
        {
            InitializeComponent();
            CenterWindowOnScreen();

            this._teamsList = teamsList;
            this.ShowTeams();
        }

        private void CenterWindowOnScreen()
        {
            double screenWidth = SystemParameters.PrimaryScreenWidth;
            double screenHeight = SystemParameters.PrimaryScreenHeight;
            double windowWidth = this.Width;
            double windowHeight = this.Height;
            this.Left = (screenWidth/2) - (windowWidth/2);
            this.Top = (screenHeight/2) - (windowHeight/2);
        }

        public void ShowTeams()
        {
            ScrollViewer sv = this.ScrollViewer;
            StackPanel s = new StackPanel();
            NameScope.SetNameScope(s, new NameScope());

            for (int i = 0; i < this._teamsList.Count; i++)
            {
                Label lab = new Label();
                lab.Name = "lab_" + i;
                lab.Content = _teamsList[i].Id + ". " + _teamsList[i].Name;
                lab.Width = 300;
                lab.Visibility = Visibility.Visible;
                lab.VerticalAlignment = VerticalAlignment.Top;
                lab.HorizontalAlignment = HorizontalAlignment.Left;

                s.Children.Add(lab);
                s.RegisterName(lab.Name, lab);
            }
            sv.Content = s;
        }

        //powrót
        private void button_Click(object sender, RoutedEventArgs e)
        {
            Start win2 = new Start(this._teamsList);
            win2.Show();
            this.Close();
        }

        //dalej
        private void button1_Click(object sender, RoutedEventArgs e)
        {
            if (this.DatePicker.SelectedDate == null)
            {
                MessageBox.Show("Nie wypełniono poprawnie daty",
                    "Błąd", MessageBoxButton.OK, MessageBoxImage.Error);
            }
            else
            {
                string tmp = this.DatePicker.SelectedDate.Value.ToShortDateString();
                string[] split = tmp.Split(".".ToCharArray());

                this._date = new DateTime(int.Parse(split[2]), int.Parse(split[1]), int.Parse(split[0]),
                    int.Parse(this.TextHour.Text), int.Parse(this.TextMinut.Text), 0);

                this._revenge = (bool)this.checkBox.IsChecked;

                GenerateTable win2 = new GenerateTable(this._teamsList, this._date, this._revenge);
                win2.Show();
                this.Close();
            }
        }

      



    }
}

