﻿using System.Linq;

namespace GeneratorTabeliBergera
{
    class Tournament
    {
        public Round[] _rounds;

        public readonly int TeamCount;

        private int RoundCount => (TeamCount % 2 == 0) ? TeamCount - 1 : TeamCount;

        public Tournament(int teamCount)
        {
            TeamCount = teamCount;
            _rounds = new Round[RoundCount];

            _rounds[0] = Round.FirstRound(TeamCount);

            for (int i = 2; i < _rounds.Length + 1; i += 2)
            {
                _rounds[i] = _rounds[i - 2].GetNextOddRound();
            }

            _rounds[RoundCount - 2] = Round.LastEvenRound(TeamCount);

            for (int i = RoundCount - 4; i >= 1; i -= 2)
            {
                _rounds[i] = _rounds[i + 2].GetPreviousEvenRound();
            }

            if (teamCount%2 == 1)
            {
                foreach (var match in _rounds.SelectMany(round => round))
                {
                    if (match.Team1Id > teamCount) match.Team1Id = 0;
                    if (match.Team2Id > teamCount) match.Team2Id = 0;
                }
            }
            
        }
    }
}
