﻿using System;
using Newtonsoft.Json;

namespace GeneratorTabeliBergera
{
    public class MatchContainer
    {
        [JsonProperty("round")]
        public int Round;
        [JsonProperty("match_number")]
        public int MatchNr;
        [JsonProperty("match_date")]
        public DateTime? Date;
        [JsonProperty("team1")]
        public string Host;
        [JsonProperty("team2")]
        public string Guest;
        [JsonProperty("pause")]
        public bool Pause;
        [JsonProperty("league")]
        public string League;

        public MatchContainer(int round, int matchNr, DateTime? date, string host, string guest, bool pause)
        {
            this.Round = round;
            this.MatchNr = matchNr;
            this.Date = date;
            this.Host = host;
            this.Guest = guest;
            this.Pause = pause;
        }

        public MatchContainer()
        { }



    }
}
