﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using Xceed.Wpf.Toolkit;

namespace GeneratorTabeliBergera
{
    /// <summary>
    /// Interaction logic for GenerateTable.xaml
    /// </summary>
    public partial class GenerateTable : Window
    {
        private readonly List<TeamContainer> _teamsList;
        private readonly DateTime? _dateTime;
        private readonly List<MatchContainer> _finalMatches;
        private readonly bool _revenge;
        
        public GenerateTable()
        {
            InitializeComponent();
            CenterWindowOnScreen();
        }

        public GenerateTable(List<TeamContainer> teamsList, DateTime? date, bool revenge)
        {
            InitializeComponent();
            CenterWindowOnScreen();
            this._revenge = revenge;
            this._finalMatches = new List<MatchContainer>();
            this._dateTime = date;
            DateTime? dateTimeTmp = this._dateTime;
            this._teamsList = teamsList;

            if (this._revenge)
            {
                this.GenerateWithRevenge(dateTimeTmp);
            }
            else
            {
                this.GenerateWithoutRevenge(dateTimeTmp);
            }
        }
        
        //generuj z rundami rewanzowymi
        private void GenerateWithRevenge(DateTime? dateTimeTmp)
        {
            Tournament tur = new Tournament(this._teamsList.Count);
            ScrollViewer sv = this.ScrollViewer;
            StackPanel s = new StackPanel();
            NameScope.SetNameScope(s, new NameScope());

            int matchNr = 1;
            int roundsCount = tur._rounds.Length;

            bool tmp = false;
            int roundNumber = 1;

            for (int i = 0; i < roundsCount; i++) //po rundach
            {
                Label round = new Label();
                round.Name = "round_" + (matchNr - 1);
                round.Content = "Runda: " + roundNumber;
                round.Width = 100;
                round.FontWeight = FontWeights.Bold;
                round.Visibility = Visibility.Visible;
                round.VerticalAlignment = VerticalAlignment.Top;
                round.HorizontalAlignment = HorizontalAlignment.Left;

                s.Children.Add(round);
                s.RegisterName(round.Name, round);

                int j = 0;
               
                foreach (var val in tur._rounds[i]._matches) //po kazdym meczu w rundzie
                {
                    StackPanel s1 = new StackPanel();
                    NameScope.SetNameScope(s1, new NameScope());
                    s1.Orientation = Orientation.Horizontal;

                    Label match = new Label();
                    match.Name = "match_" + i + "_" + j;

                    string host = "";
                    string guest = "";
                    
                    if (tmp == false)
                    {
                        host = val.Team1Id == 0 ? "pauzuje" : this._teamsList.Find(x => x.Id == val.Team1Id).Name;
                        guest = val.Team2Id == 0 ? "pauzuje" : this._teamsList.Find(x => x.Id == val.Team2Id).Name;
                    }
                    else
                    {
                        guest = val.Team1Id == 0 ? "pauzuje" : this._teamsList.Find(x => x.Id == val.Team1Id).Name;
                        host = val.Team2Id == 0 ? "pauzuje" : this._teamsList.Find(x => x.Id == val.Team2Id).Name;
                    }

                    if(host == "pauzuje" || guest == "pauzuje")
                        match.Content = host + " - " + guest;
                    else
                        match.Content = matchNr + ". " + host + " - " + guest;

                    match.Width = 380;
                    match.Visibility = Visibility.Visible;
                    match.VerticalAlignment = VerticalAlignment.Top;
                    match.HorizontalAlignment = HorizontalAlignment.Left;

                    s1.Children.Add(match);
                    s1.RegisterName(match.Name, match);
                    j++;

                    DateTimePicker dp = new DateTimePicker();
                    dp.Value = dateTimeTmp;

                    bool pause;

                    if (val.Team1Id != 0 && val.Team2Id != 0)
                        pause = false;
                    else
                        pause = true;

                    MatchContainer mc = new MatchContainer(roundNumber, matchNr, dateTimeTmp, host, guest, pause);
                    this._finalMatches.Add(mc);

                    if (val.Team1Id != 0 && val.Team2Id != 0)
                    {
                        dp.Format = DateTimeFormat.Custom;
                        dp.FormatString = "ddd dd-MM-yyyy HH:mm";
                        dp.Width = 160;
                        s1.Children.Add(dp);
                        matchNr++;
                    }
                    else
                    {
                        match.FontStyle = FontStyles.Italic;
                        match.Foreground = new SolidColorBrush(Colors.Firebrick);
                    }
                    s.Children.Add(s1);
                    
                }
                
                dateTimeTmp = dateTimeTmp?.AddDays(7);
                roundNumber++;

                if (i == roundsCount - 1 && tmp == false)
                {
                    i = -1;
                    tmp = true;
                }
            }
            sv.Content = s;
        }

        //generuj bez rund rewanzowych
        private void GenerateWithoutRevenge(DateTime? dateTimeTmp)
        {
            Tournament tur = new Tournament(this._teamsList.Count);
            ScrollViewer sv = this.ScrollViewer;
            StackPanel s = new StackPanel();
            NameScope.SetNameScope(s, new NameScope());

            int matchNr = 1;
            int roundsCount = tur._rounds.Length;
            
            for (int i = 0; i < roundsCount; i++) //po rundach
            {
                Label round = new Label();
                round.Name = "round_" + i;
                round.Content = "Runda: " + tur._rounds[i].RoundNumber;
                round.Width = 100;
                round.FontWeight = FontWeights.Bold;
                round.Visibility = Visibility.Visible;
                round.VerticalAlignment = VerticalAlignment.Top;
                round.HorizontalAlignment = HorizontalAlignment.Left;

                s.Children.Add(round);
                s.RegisterName(round.Name, round);

                int j = 0;

                foreach (var val in tur._rounds[i]._matches) //po kazdym meczu w rundzie
                {
                    StackPanel s1 = new StackPanel();
                    NameScope.SetNameScope(s1, new NameScope());
                    s1.Orientation = Orientation.Horizontal;

                    Label match = new Label();
                    match.Name = "match_" + i + "_" + j;

                    var host = val.Team1Id == 0 ? "pauzuje" : this._teamsList.Find(x => x.Id == val.Team1Id).Name;
                    var guest = val.Team2Id == 0 ? "pauzuje" : this._teamsList.Find(x => x.Id == val.Team2Id).Name;

                    if (host == "pauzuje" || guest == "pauzuje")
                        match.Content = host + " - " + guest;
                    else
                        match.Content = matchNr + ". " + host + " - " + guest;

                    match.Width = 380;
                    match.Visibility = Visibility.Visible;
                    match.VerticalAlignment = VerticalAlignment.Top;
                    match.HorizontalAlignment = HorizontalAlignment.Left;

                    s1.Children.Add(match);
                    s1.RegisterName(match.Name, match);
                    j++;

                    DateTimePicker dp = new DateTimePicker();
                    dp.Value = dateTimeTmp;

                    bool pause;

                    if (val.Team1Id != 0 && val.Team2Id != 0)
                        pause = false;
                    else
                        pause = true;

                    MatchContainer mc = new MatchContainer(tur._rounds[i].RoundNumber, matchNr, dateTimeTmp, host, guest, pause);
                    this._finalMatches.Add(mc);

                    if (val.Team1Id != 0 && val.Team2Id != 0)
                    {
                        dp.Format = DateTimeFormat.Custom;
                        dp.FormatString = "ddd dd-MM-yyyy HH:mm";
                        dp.Width = 160;
                        s1.Children.Add(dp);
                        matchNr++;
                    }
                    else
                    {
                        match.FontStyle = FontStyles.Italic;
                        match.Foreground = new SolidColorBrush(Colors.Firebrick);
                    }
                    s.Children.Add(s1);

                }
                dateTimeTmp = dateTimeTmp?.AddDays(7);
                
            }
            sv.Content = s;
        }
        
        private void CenterWindowOnScreen()
        {
            double screenWidth = SystemParameters.PrimaryScreenWidth;
            double screenHeight = SystemParameters.PrimaryScreenHeight;
            double windowWidth = this.Width;
            double windowHeight = this.Height;
            this.Left = (screenWidth / 2) - (windowWidth / 2);
            this.Top = (screenHeight / 2) - (windowHeight / 2);
        }

        //reset
        private void Reset(object sender, RoutedEventArgs e)
        {
            Start win2 = new Start(this._teamsList);
            win2.Show();
            this.Close();
        }

        //dalej
        private void Next(object sender, RoutedEventArgs e)
        {
            FinalWindow win2 = new FinalWindow(this._finalMatches);
            win2.Show();
            this.Close();
        }
    }
}
