﻿namespace GeneratorTabeliBergera
{
    class Match
    {
        public int Team1Id;
        public int Team2Id;

        private Match()
        {
            
        }

        public Match(int team1Id, int team2Id)
        {
            Team1Id = team1Id;
            Team2Id = team2Id;
        }

        public Match NextNextRoundMatch(int teamCount)
        {
            var count = (teamCount%2 == 0) ? teamCount : teamCount + 1;

            return new Match
            {
                Team1Id = (Team1Id + 1 == count) ? 1 : Team1Id + 1,
                Team2Id = (Team2Id + 1 == count) ? 1 : Team2Id + 1
            };
        }

        public Match PreviousPreviousRoundMatch(int teamCount)
        {
            var count = (teamCount % 2 == 0) ? teamCount : teamCount + 1;

            return new Match
            {
                Team1Id = (Team1Id - 1 == 0) ? count - 1 : Team1Id - 1,
                Team2Id = (Team2Id - 1 == 0) ? count - 1 : Team2Id - 1
            };
        }

        public override string ToString() => $"{Team1Id}, {Team2Id}";
    }
}
