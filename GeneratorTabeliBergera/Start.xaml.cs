﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Windows;
using System.Windows.Controls;
using Microsoft.Win32;

namespace GeneratorTabeliBergera
{
    /// <summary>
    /// Interaction logic for Start.xaml
    /// </summary>
    public partial class Start : Window
    {
        public int Count = 0;
        private List<TeamContainer> _teamsList;

        public Start()
        {
            InitializeComponent();
            CenterWindowOnScreen();
        }

        public Start(List<TeamContainer> teamsList)
        {
            InitializeComponent();
            CenterWindowOnScreen();

            this.Initialize(teamsList);
        }

        public void Initialize(List<TeamContainer> teamsList)
        {
            this.Clear();
            this._teamsList = teamsList;
            this.Count = _teamsList.Count;
            this.GenerateControls();
            this.CompleteControls();
        }

        private void CenterWindowOnScreen()
        {
            double screenWidth = SystemParameters.PrimaryScreenWidth;
            double screenHeight = SystemParameters.PrimaryScreenHeight;
            double windowWidth = this.Width;
            double windowHeight = this.Height;
            this.Left = (screenWidth / 2) - (windowWidth / 2);
            this.Top = (screenHeight / 2) - (windowHeight / 2);
        }

        //wprowadz
        private void button_Click(object sender, RoutedEventArgs e)
        {
            if (!Regex.IsMatch(textBox.Text, @"^\d+$") || string.IsNullOrWhiteSpace(textBox.Text))
            {
                MessageBox.Show("Nie wypełniono poprawnie pola !",
                    "Błąd", MessageBoxButton.OK, MessageBoxImage.Error);
            }
            else
            {
                this.Clear();
                this.Count = int.Parse(this.textBox.Text);
                this.GenerateControls();
            }
        }

        //dalej
        private void button2_Click(object sender, RoutedEventArgs e)
        {
            this._teamsList = new List<TeamContainer>();
            ScrollViewer sv = this.ScrollVewer;
            StackPanel s = (sv.Content as StackPanel);

            if (s == null || this.CheckTextboxes(s))
            {
                MessageBox.Show("Nie wypełniono poprawnie pola",
                    "Błąd", MessageBoxButton.OK, MessageBoxImage.Error);
            }
            else
            {
                int tmpit = 1;

                for (int i = 0; i < this.Count; i += 5)
                {
                    foreach (var child in s.Children)
                    {
                        if (child is TextBox)
                        {
                            TeamContainer tmp = new TeamContainer();
                            tmp.Name = (child as TextBox).Text.Trim();
                            tmp.Id = tmpit;
                            _teamsList.Add(tmp);
                            tmpit++;
                        }
                    }
                }

                if (this._teamsList.GroupBy(n => n.Name).Any(c => c.Count() > 1))
                {
                    MessageBox.Show("Nie moża wprowadzać duplikatów !",
                        "Błąd", MessageBoxButton.OK, MessageBoxImage.Error);
                }
                else
                {
                    Teams win2 = new Teams(this._teamsList);
                    win2.Show();
                    this.Close();
                }

            }
        }

        public void GenerateControls()
        {
            ScrollViewer sv = this.ScrollVewer;
            StackPanel s = new StackPanel();
            NameScope.SetNameScope(s, new NameScope());

            for (int i = 0; i < this.Count; i++)
            {
                Label lab = new Label();
                lab.Name = "lab_" + i;
                lab.Content = "Zespół " + (i + 1) + ": ";
                lab.Width = 100;
                lab.Visibility = Visibility.Visible;
                lab.VerticalAlignment = VerticalAlignment.Top;
                lab.HorizontalAlignment = HorizontalAlignment.Left;

                var tmpLabel = s.FindName(lab.Name);
                if ((tmpLabel as Label) != null)
                {
                    NameScope.GetNameScope(this).UnregisterName(lab.Name);
                }

                s.Children.Add(lab);
                s.RegisterName(lab.Name, lab);

                //

                TextBox txtNumber = new TextBox();
                txtNumber.Width = 220;
                txtNumber.Name = "n" + (i + 1);
                txtNumber.VerticalAlignment = VerticalAlignment.Top;
                txtNumber.HorizontalAlignment = HorizontalAlignment.Left;

                var tmpTxt = s.FindName(txtNumber.Name);
                if ((tmpTxt as TextBox) != null)
                {
                    NameScope.GetNameScope(this).UnregisterName(txtNumber.Name);
                }

                s.Children.Add(txtNumber);
                s.RegisterName(txtNumber.Name, txtNumber);
            }

            sv.Content = s;
        }

        //reset
        private void button3_Click(object sender, RoutedEventArgs e)
        {
            this.Clear();
        }

        public void Clear()
        {
            ScrollViewer sv = this.ScrollVewer;
            StackPanel s = (sv.Content as StackPanel);
            s?.Children.Clear();
            sv.Content = null;

            this.Count = 0;
            this._teamsList?.Clear();
            this.UpdateLayout();
        }

        public void CompleteControls()
        {

            ScrollViewer sv = this.ScrollVewer;
            StackPanel s = (sv.Content as StackPanel);

            for (int i = 0; i < this.Count; i++)
            {
                var tmpTxt = s.FindName("n" + (i + 1));
                (tmpTxt as TextBox).Text = _teamsList[i].Name;

            }
        }

        public bool CheckTextboxes(StackPanel s)
        {
            return s.Children.OfType<TextBox>().Any(child => string.IsNullOrWhiteSpace((child as TextBox).Text));
        }
        
        //wczytaj z pliku
        private void LoadTeamsButton_Click(object sender, RoutedEventArgs e)
        {
            this.Clear();
            List<TeamContainer> list = new List<TeamContainer>();

            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.DefaultExt = ".txt";
            openFileDialog.Filter = "TXT Files (*.txt)|*.txt";

            int it = 1;

            if (openFileDialog.ShowDialog() == true)
            {
                try
                {
                    string filename = openFileDialog.FileName;
                    string[] filelines = File.ReadAllLines(filename);

                    for (int a = 0; a < filelines.Length; a++)
                    {
                        string line = filelines[a].Trim();
                        if (line == "") continue;

                        TeamContainer tmp = new TeamContainer();
                        tmp.Id = it;
                        tmp.Name = line;
                        list.Add(tmp);
                        it++;
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Error: Could not read file from disk. Original error: " + ex.Message);
                }
            }
            this.Initialize(list);
        }





    }
}
