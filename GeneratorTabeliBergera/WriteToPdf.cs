﻿using System;
using System.Windows.Media;
using iTextSharp.text;
using iTextSharp.text.pdf;

namespace GeneratorTabeliBergera
{
    class WriteToPdf
    {
        public void WriteTitlePdf(Document pdfDoc, string titleString)
        {
            Font titleFont = FontFactory.GetFont(FontFactory.HELVETICA, BaseFont.CP1257, 32);
            Paragraph title = new Paragraph(titleString, titleFont);
            title.Alignment = Element.ALIGN_CENTER;
            pdfDoc.Add(title);
        }

        public void WriteRoundPdf(Document pdfDoc, PdfPTable table, int round)
        {
            PdfPCell cell = new PdfPCell();
            cell.Colspan = 4;
            cell.HorizontalAlignment = 1; //0=Left, 1=Centre, 2=Right

            Font roundFont = FontFactory.GetFont(FontFactory.HELVETICA_BOLD, BaseFont.CP1257, 12);
            Paragraph roundPar = new Paragraph("Kolejka: " + round, roundFont);
            roundPar.Alignment = Element.ALIGN_CENTER;

            cell.AddElement(roundPar);
            table.AddCell(cell);
        }

        public void WriteMatchPdf(Document pdfDoc, PdfPTable table, int matchNr, DateTime? date, string host, string guest, bool pause)
        {
            string dateString = date.Value.ToString("g");
            Font roundFont = FontFactory.GetFont(FontFactory.HELVETICA, BaseFont.CP1257, 12);
            PdfPCell cellMatchNr = new PdfPCell();

            if (!pause)
            {
                Paragraph matchNrPar = new Paragraph(matchNr + ".", roundFont);
                matchNrPar.Alignment = Element.ALIGN_CENTER;
                cellMatchNr.HorizontalAlignment = Element.ALIGN_CENTER;
                cellMatchNr.VerticalAlignment = Element.ALIGN_CENTER;
                cellMatchNr.AddElement(matchNrPar);
            }
            else
            {
                roundFont.Color = new BaseColor(Colors.Firebrick.R, Colors.Firebrick.G, Colors.Firebrick.B);
            }

            PdfPCell cellhost = new PdfPCell();
            Paragraph hostPar = new Paragraph(host, roundFont);
            hostPar.Alignment = Element.ALIGN_CENTER;
            cellhost.HorizontalAlignment = Element.ALIGN_CENTER;
            cellhost.VerticalAlignment = Element.ALIGN_MIDDLE;
            cellhost.AddElement(hostPar);

            PdfPCell cellguest = new PdfPCell();
            Paragraph guestPar = new Paragraph(guest, roundFont);
            guestPar.Alignment = Element.ALIGN_CENTER;
            cellguest.HorizontalAlignment = Element.ALIGN_CENTER;
            cellguest.VerticalAlignment = Element.ALIGN_MIDDLE;
            cellguest.AddElement(guestPar);

            PdfPCell celldate = new PdfPCell();
            Paragraph datePar = new Paragraph(dateString, roundFont);
            datePar.Alignment = Element.ALIGN_CENTER;
            celldate.HorizontalAlignment = Element.ALIGN_CENTER;
            celldate.VerticalAlignment = Element.ALIGN_MIDDLE;
            celldate.AddElement(datePar);

            table.AddCell(cellMatchNr);
            table.AddCell(cellhost);
            table.AddCell(cellguest);
            table.AddCell(celldate);
        }

        public void WriteHeaderPdf(Document pdfDoc, PdfPTable table)
        {
            Font roundFont = FontFactory.GetFont(FontFactory.HELVETICA_BOLD, BaseFont.CP1257, 12);

            PdfPCell cellMatchNr = new PdfPCell();
            Paragraph matchNrPar = new Paragraph("Nr", roundFont);
            matchNrPar.Alignment = Element.ALIGN_CENTER;
            cellMatchNr.HorizontalAlignment = Element.ALIGN_CENTER;
            cellMatchNr.VerticalAlignment = Element.ALIGN_CENTER;
            cellMatchNr.AddElement(matchNrPar);

            PdfPCell cellhost = new PdfPCell();
            Paragraph hostPar = new Paragraph("Gospodarz", roundFont);
            hostPar.Alignment = Element.ALIGN_CENTER;
            cellhost.HorizontalAlignment = Element.ALIGN_CENTER;
            cellhost.VerticalAlignment = Element.ALIGN_MIDDLE;
            cellhost.AddElement(hostPar);

            PdfPCell cellguest = new PdfPCell();
            Paragraph guestPar = new Paragraph("Gość", roundFont);
            guestPar.Alignment = Element.ALIGN_CENTER;
            cellguest.HorizontalAlignment = Element.ALIGN_CENTER;
            cellguest.VerticalAlignment = Element.ALIGN_MIDDLE;
            cellguest.AddElement(guestPar);

            PdfPCell celldate = new PdfPCell();
            Paragraph datePar = new Paragraph("Data i Godzina", roundFont);
            datePar.Alignment = Element.ALIGN_CENTER;
            celldate.HorizontalAlignment = Element.ALIGN_CENTER;
            celldate.VerticalAlignment = Element.ALIGN_MIDDLE;
            celldate.AddElement(datePar);

            table.AddCell(cellMatchNr);
            table.AddCell(cellhost);
            table.AddCell(cellguest);
            table.AddCell(celldate);
        }

    }
}
