﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text.RegularExpressions;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using Microsoft.Win32;
using iTextSharp.text;
using iTextSharp.text.pdf;
using Newtonsoft.Json;

namespace GeneratorTabeliBergera
{
    /// <summary>
    /// Interaction logic for FinalWindow.xaml
    /// </summary>
    public partial class FinalWindow : Window
    {
        private readonly List<MatchContainer> _matchContainer;

        public FinalWindow()
        {
            InitializeComponent();
            CenterWindowOnScreen();
        }

        public FinalWindow(List<MatchContainer> matchContainer)
        {
            InitializeComponent();
            CenterWindowOnScreen();
            this._matchContainer = matchContainer;

            ScrollViewer sv = this.ScrollViewer;
            StackPanel s = new StackPanel();
            NameScope.SetNameScope(s, new NameScope());

            int roundTmp = 0;

            for (int i = 0; i < this._matchContainer.Count; i++)
            {
                if (roundTmp != this._matchContainer[i].Round)
                {
                    roundTmp = this._matchContainer[i].Round;

                    Label round = new Label();
                    round.Name = "round_" + i;
                    round.Content = "Runda: " + roundTmp;
                    round.Width = 100;
                    round.FontWeight = FontWeights.Bold;
                    round.Visibility = Visibility.Visible;
                    round.VerticalAlignment = VerticalAlignment.Top;
                    round.HorizontalAlignment = HorizontalAlignment.Left;

                    s.Children.Add(round);
                    s.RegisterName(round.Name, round);
                }
                
                StackPanel s1 = new StackPanel();
                NameScope.SetNameScope(s1, new NameScope());
                s1.Orientation = Orientation.Horizontal;

                Label match = new Label();
                match.Name = "match_" + i;

                if (this._matchContainer[i].Pause)
                    match.Content = this._matchContainer[i].Host + " - " + this._matchContainer[i].Guest;
                else
                    match.Content = this._matchContainer[i].MatchNr + ". " + this._matchContainer[i].Host + " - " + this._matchContainer[i].Guest;

                match.Width = 380;
                match.Visibility = Visibility.Visible;
                match.VerticalAlignment = VerticalAlignment.Top;
                match.HorizontalAlignment = HorizontalAlignment.Left;

                s1.Children.Add(match);
                s1.RegisterName(match.Name, match);

                Label matchDate = new Label();
                matchDate.Name = "matchDate_" + i;
                matchDate.Content = this._matchContainer[i].Date.Value.ToString("g");
                matchDate.Width = 180;
                matchDate.Visibility = Visibility.Visible;
                matchDate.VerticalAlignment = VerticalAlignment.Top;
                matchDate.HorizontalAlignment = HorizontalAlignment.Left;

                s1.Children.Add(matchDate);
                s1.RegisterName(matchDate.Name, matchDate);

                if (this._matchContainer[i].Pause)
                {
                    match.FontStyle = FontStyles.Italic;
                    match.Foreground = new SolidColorBrush(Colors.Firebrick);
                }

                s.Children.Add(s1);

            }
            sv.Content = s;

        }

        private void CenterWindowOnScreen()
        {
            double screenWidth = SystemParameters.PrimaryScreenWidth;
            double screenHeight = SystemParameters.PrimaryScreenHeight;
            double windowWidth = this.Width;
            double windowHeight = this.Height;
            this.Left = (screenWidth / 2) - (windowWidth / 2);
            this.Top = (screenHeight / 2) - (windowHeight / 2);
        }

        //zapisz do pdf
        private void ExportPdf(object sender, RoutedEventArgs e)
        {
            if (string.IsNullOrWhiteSpace(this.TextBox.Text))
            {
                MessageBox.Show("Nie podano nazwy kategorii !",
                    "Błąd", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }

            WriteToPdf wtp = new WriteToPdf();
            SaveFileDialog sfd = new SaveFileDialog();
            sfd.Filter = "Pdf File |*.pdf";
            sfd.FileName = this.TextBox.Text;
            bool? result = sfd.ShowDialog();
            try
            {
                if (result ?? false)
                {
                    Document doc = new Document(PageSize.A4, 10, 10, 42, 35);
                    //PdfWriter wri = PdfWriter.GetInstance(doc, new FileStream(path + "_Text.pdf", FileMode.Create));
                    PdfWriter wri = PdfWriter.GetInstance(doc, new FileStream(sfd.FileName, FileMode.Create));
                    doc.Open();
                    wtp.WriteTitlePdf(doc, Path.GetFileNameWithoutExtension(sfd.FileName));
                    int roundTmp = 0;
                    PdfPTable table = new PdfPTable(4);
                    table.TotalWidth = PageSize.A4.Width - 20;
                    table.LockedWidth = true;
                    table.HorizontalAlignment = Element.ALIGN_CENTER;
                    float[] widths = new float[] { 0.8f, 8f, 8f, 3.5f };
                    table.SetWidths(widths);
                    table.SpacingBefore = 20f;
                    table.SpacingAfter = 20f;

                    wtp.WriteHeaderPdf(doc, table);

                    this.SaveLeagueName();

                    foreach (var val in this._matchContainer)
                    {
                        if (roundTmp != val.Round)
                        {
                            roundTmp = val.Round;
                            wtp.WriteRoundPdf(doc, table, roundTmp);
                        }

                        //zapis druzyn
                        wtp.WriteMatchPdf(doc, table, val.MatchNr, val.Date, val.Host, val.Guest, val.Pause);
                    }

                    doc.Add(table);
                    doc.Close();
                    MessageBox.Show("Plik zapisano", "Zapis", MessageBoxButton.OK, MessageBoxImage.Information);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: Nie można zapisąc pliku. Original error: " + ex.Message);
            }
        }

        //zapisz do json
        private void ExportJson(object sender, RoutedEventArgs e)
        {
            if (string.IsNullOrWhiteSpace(this.TextBox.Text))
            {
                MessageBox.Show("Nie podano nazwy kategorii !",
                    "Błąd", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }

            SaveFileDialog sfd = new SaveFileDialog();
            sfd.Filter = "JSON File |*.json";
            sfd.FileName = this.TextBox.Text;
            bool? result = sfd.ShowDialog();
            try
            {
                if (result ?? false)
                {
                    this.SaveLeagueName();
                    var json = JsonConvert.SerializeObject(new { competation = _matchContainer });
                    File.WriteAllText(sfd.FileName, json);
                    MessageBox.Show("Plik zapisano", "Zapis", MessageBoxButton.OK, MessageBoxImage.Information);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: Nie można zapisąc pliku. Original error: " + ex.Message);
            }
        }


        public void SaveLeagueName()
        {
            string league = this.TextBox.Text;

            foreach (var val in this._matchContainer)
            {
                val.League = league;
            }
        }

    }
}
