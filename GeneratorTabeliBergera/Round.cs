﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace GeneratorTabeliBergera
{
    class Round : IEnumerable<Match>
    {
        public readonly int TeamCount;
        public readonly int RoundNumber;
        public readonly Match[] _matches;

        public Match this[int index] => _matches[index];

        private Round(int teamCount, int roundNumber)
        {
            TeamCount = teamCount;
            RoundNumber = roundNumber;

            int matchCount = teamCount / 2;
            if (teamCount % 2 != 0) matchCount++;

            _matches = new Match[matchCount];
        }

        public static Round FirstRound(int teamCount)
        {
            var count = (teamCount%2 == 0) ? teamCount : teamCount + 1;

            var firstRound = new Round(teamCount, 1);
            firstRound._matches[0] = new Match(1, count);

            for (int i = 1; i < firstRound._matches.Length; i++)
            {
                firstRound._matches[i] = new Match(firstRound._matches[i - 1].Team1Id + 1, firstRound._matches[i - 1].Team2Id - 1);

                if (firstRound._matches[i].Team1Id == count) firstRound._matches[i].Team1Id = 1;
                if (firstRound._matches[i].Team2Id == count) firstRound._matches[i].Team2Id = 1;
            }

            return firstRound;
        }

        public static Round LastEvenRound(int teamCount)
        {
            var count = (teamCount % 2 == 0) ? teamCount : teamCount + 1;

            var lastEvenRound = new Round(teamCount, count - 2);
            lastEvenRound._matches[0] = new Match(count, count - 1);

            for (int i = 1; i < lastEvenRound._matches.Length; i++)
            {
                lastEvenRound._matches[i] = new Match(lastEvenRound._matches[i - 1].Team1Id + 1, lastEvenRound._matches[i - 1].Team2Id - 1);

                if (lastEvenRound._matches[i].Team1Id > count) lastEvenRound._matches[i].Team1Id = 1;
                if (lastEvenRound._matches[i].Team2Id == count) lastEvenRound._matches[i].Team2Id = 1;
            }

            return lastEvenRound;
        }

        public Round GetNextOddRound()
        {
            var nextRound = new Round(TeamCount, RoundNumber + 2);
            var count = (TeamCount % 2 == 0) ? TeamCount : TeamCount + 1;

            var firstMatch = _matches[0].NextNextRoundMatch(count);
            firstMatch.Team2Id = count;
            nextRound._matches[0] = firstMatch;

            for (int i = 1; i < _matches.Length; i++)
            {
                nextRound._matches[i] = _matches[i].NextNextRoundMatch(count);
            }

            return nextRound;
        }

        public Round GetPreviousEvenRound()
        {
            var prevRound = new Round(TeamCount, RoundNumber - 2);
            var count = (TeamCount % 2 == 0) ? TeamCount : TeamCount + 1;

            var firstMatch = _matches[0].PreviousPreviousRoundMatch(count);
            firstMatch.Team1Id = count;
            prevRound._matches[0] = firstMatch;

            for (int i = 1; i < _matches.Length; i++)
            {
                prevRound._matches[i] = _matches[i].PreviousPreviousRoundMatch(count);
            }

            return prevRound;
        }

        public IEnumerator<Match> GetEnumerator() => (IEnumerator<Match>)_matches.AsEnumerable().GetEnumerator();

        IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();
    }
}
